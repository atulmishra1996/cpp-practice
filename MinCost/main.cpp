#include "iostream"
#include "queue"
#include "vector"
using namespace  std;

class Travel {

public:
    int cost;
    int j;
    int bi;
    int ai;

    Travel(int cost, int j, int ai, int bi) {
        this->ai = ai;
        this->cost = cost;
        this->bi = bi;
        this-> j = j;
    }

    bool operator<(const Travel& other) const {
        if(cost == other.cost){
            return j > other.j;
        }

        return cost > other.cost;
    }
};

class Solution {

public:
    int getMinCost(vector<int> &a, vector<int> &b, int numberOfItemsToPurchase) {
        int aSize = a.size();

        priority_queue<Travel> minCostQueue;

        for (int i = 0; i < aSize; i++) {
            int totalCost = a[i];
            Travel travel = *new Travel(totalCost, 2, a[i], b[i]);
            minCostQueue.push(travel);
        }

        int count = 0;
        int totalAns = 0;
        while(!minCostQueue.empty() && count < numberOfItemsToPurchase){
            Travel travel = minCostQueue.top();
            totalAns += travel.cost;
            minCostQueue.pop();

            int nextCost = travel.ai + ( (travel.j - 1) * travel.bi);
            Travel travel1 = *new Travel(nextCost, travel.j+1, travel.ai, travel.bi);
            minCostQueue.push(travel1);
            count = count + 1;
        }
        return totalAns;
    }
};

int main() {
    vector<int> a = {5, 2, 6};
    vector<int> b = {1, 2, 10};
    int numberOfItems = 5;
    Solution solution = * new Solution();
    int ans = solution.getMinCost(a, b, numberOfItems);
    cout<<ans;
}