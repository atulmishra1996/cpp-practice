//
// Created by Atul Mishra on 22/07/23.
//

#include "iostream"
#include "vector"
#include "stack"
using namespace std;

vector<int> getRemainingAsteriods(vector<int>& asteroids){
    stack<int> asteroidsStack;
    asteroidsStack.push(asteroids[0]);
    for(int i=1;i<asteroids.size();i++){
        int currentAsteroid = asteroids[i];
        int winnerAsteroid = INT_MIN;
        if(currentAsteroid < 0 ){
            cout<<"CHecking :: "<<currentAsteroid<<endl;
            while(!asteroidsStack.empty() && asteroidsStack.top() > 0){
                cout<<"Getting :: "<<asteroidsStack.top()<<endl;
                int stackAsteroid = asteroidsStack.top();
                if(abs(currentAsteroid) > stackAsteroid){
                    asteroidsStack.pop();
                    winnerAsteroid = currentAsteroid;
                } else if(asteroidsStack.top() > abs(currentAsteroid)) {
                    break;
                }
            }
            if(winnerAsteroid != INT_MIN){
                asteroidsStack.push(winnerAsteroid);
            }
        } else {
            asteroidsStack.push(currentAsteroid);
        }
    }

    vector<int> result;
    while (!asteroidsStack.empty()){
        result.push_back(asteroidsStack.top());
        asteroidsStack.pop();
    }


    reverse(result.begin(), result.end());
    return result;
}

int main(){
    vector<int> asteriods = {10 , 2, -8};
    vector<int> remainingAsteriods = getRemainingAsteriods(asteriods);
    for(int i: remainingAsteriods){
        cout<<i<<" : ";
    }
}