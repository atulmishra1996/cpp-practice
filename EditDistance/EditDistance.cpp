//
// Created by Atul Mishra on 20/07/23.
//

#include "iostream"
#include "string"
using namespace std;

int editDistance(string& word1, string& word2, int m, int n){

    //base condition
    if(m == 0) return n;
    if(n == 0) return m;

    if(word1[m-1] == word2[n-1])
        return editDistance(word1, word2, m-1, n-1);
    else{
        int insertChar = editDistance(word1, word2, m, n-1);
        int deleteChar = editDistance(word1, word2, m-1, n);
        int replaceChar = editDistance(word1, word2, m-1, n-1);

        return 1 + min({insertChar, deleteChar, replaceChar});
    }
}

int findMinimumStepsToConvertAIntoB(string &a, string &b, int aIndex, int bIndex, vector<vector<int>> &dp){
        if(aIndex == 0 || bIndex == 0){
            return aIndex + bIndex;
        }

        if(dp[aIndex][bIndex] != -1){
            return dp[aIndex][bIndex];
        }

        if(a[aIndex-1] == b[bIndex-1]){
            return dp[aIndex][bIndex] = findMinimumStepsToConvertAIntoB(a,b,aIndex-1,bIndex-1,dp);
        } else {
            return dp[aIndex][bIndex] = 1 + min (
                    {
                            findMinimumStepsToConvertAIntoB(a, b, aIndex, bIndex-1,dp),
                            findMinimumStepsToConvertAIntoB(a, b, aIndex-1, bIndex,dp),
                            findMinimumStepsToConvertAIntoB(a, b,aIndex-1,bIndex-1,dp)
                    });
        }
    }


int main(){
    string a = "dinitrophenylhydrazine";
    string b = "benzalphenylhydrazone";
    int n = a.length();
    int m = b.length();
    vector<vector<int>> dp(n+1 , vector<int>(m+1, -1));
    int minStepsRequired = findMinimumStepsToConvertAIntoB(a,b,a.length(),b.length(), dp);
    cout<<minStepsRequired;
}