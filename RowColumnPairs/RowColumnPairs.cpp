//
// Created by Atul Mishra on 23/07/23.
//

#include "iostream"
#include "map"
using namespace std;

class TrieNode {
public:
    int count = 0;
    unordered_map<int, TrieNode*> trieMap;

    TrieNode(){
        this->count = 0;
        this-> trieMap.clear();
    }
};

class Trie {
public:
    TrieNode* trieNode;

    Trie(){
        trieNode = new TrieNode();
    }

    void addArray(vector<int> row){
        TrieNode* currentNode = trieNode;
        for(int i: row){
            if(currentNode->trieMap.find(i) == currentNode->trieMap.end()){
                currentNode->trieMap[i] = new TrieNode();
            }

            currentNode = currentNode->trieMap[i];
        }

        currentNode->count++;
    }

    int search(vector<int> column){

        cout<<"Trying to search "<<endl;
        for(int i: column){
            cout<<i<<" ";
        }
        cout<<endl;

        TrieNode* currentNode = trieNode;
        for(int i: column){
            if(currentNode->trieMap.find(i) == currentNode->trieMap.end()){
                cout<<"returning as not found -> "<<i<<endl;
                return 0;
            }

            cout<<"Found -> "<<i<<endl;
            currentNode = currentNode->trieMap[i];
        }

        return currentNode->count;
    }
};

int main(){
    vector<vector<int>> grid = {
        {3,2,1},
        {1,7,6},
        {2,7,7}
    };

    Trie trie;
    for(vector<int> row: grid){
        trie.addArray(row);
    }
    int rows = grid.size();
    int columns = grid[0].size();

    int ans = 0;
    for(int i=0; i<columns;i++) {
        vector<int> currentColumn;
        for (int j = 0; j < rows; j++) {
            currentColumn.push_back(grid[j][i]);
        }

        ans = ans + trie.search(currentColumn);
    }

    cout<<ans;
}