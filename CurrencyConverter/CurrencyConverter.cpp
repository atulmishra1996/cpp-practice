//
// Created by Atul Mishra on 23/07/23.
//

/*
 *  Given currency conversion data of two countries
 *   - format - {"USD", "INR", "81.5"} -> 1 USD = 81.5 INR or 1 INR = 1/81.5 USD
 *   find conversion fromCountry -> toCountry of Value(V)
 *    - if can find the conversion, return -1
 */

#include "iostream"
#include "vector"
#include "map"
#include "queue"
#include "tuple"

using namespace std;

class CurrencyConversionData {
public:
    string toCountry;
    float conversionValue;

    CurrencyConversionData(string country2, float value){
        toCountry = country2;
        conversionValue = value;
    }
};

class CurrencyConversionCalculator{
public:
    unordered_map<string, vector<CurrencyConversionData>> currencyConversionMap;

    CurrencyConversionCalculator(){
        currencyConversionMap.clear();
    }

    void addCurrenyConversionData(string fromCountry, string toCountry, float value){
        CurrencyConversionData currencyConversionData = *new CurrencyConversionData(toCountry, value);
        if(currencyConversionMap.find(fromCountry) == currencyConversionMap.end()){
            vector<CurrencyConversionData> currenyConvertorList;
            currenyConvertorList.push_back(currencyConversionData);
            currencyConversionMap[fromCountry] = currenyConvertorList;
        } else {
            vector<CurrencyConversionData> currenyConvertorList = currencyConversionMap[fromCountry];
            currenyConvertorList.push_back(currencyConversionData);
            currencyConversionMap[fromCountry] = currenyConvertorList;
        }
    }

    void addCurrencyConversion(string fromCountry, string toCountry, float value){
        addCurrenyConversionData(fromCountry, toCountry, value);
        addCurrenyConversionData(toCountry, fromCountry, float(1/value));
    }


    float findCurrencyConversion(string fromCountry, string toCountry, float value){
        priority_queue<pair<float,string>> currencyConversionQueue;
        unordered_map<string, bool> traversedCurrenyMap;

        currencyConversionQueue.push(make_pair(value,fromCountry));
        traversedCurrenyMap[fromCountry] = true;

        while (!currencyConversionQueue.empty()){
            pair<float, string> currentCurrencyPair = currencyConversionQueue.top();
            float currentValue = currentCurrencyPair.first;
            string travelCountry = currentCurrencyPair.second;

            if(travelCountry == toCountry){
                return currentValue;
            }

            currencyConversionQueue.pop();

            if(currencyConversionMap.find(travelCountry) != currencyConversionMap.end()){
                vector<CurrencyConversionData> linkedCountriesList = currencyConversionMap[travelCountry];

                for(CurrencyConversionData currencyConversionData: linkedCountriesList){
                    string toCountry = currencyConversionData.toCountry;
                    float val  = currencyConversionData.conversionValue;

                    if(traversedCurrenyMap.find(toCountry) == traversedCurrenyMap.end()){
                        float exchangedValue = float(currentValue * val);
                        currencyConversionQueue.push(make_pair(exchangedValue, toCountry));
                        traversedCurrenyMap[toCountry] = true;
                    }
                }
            }
        }

        return -1;
    }
};


int main(){
    using converterTuple = tuple<string, string, float>;
    vector<converterTuple> currencyDataList;

    currencyDataList.push_back(make_tuple("USD", "INR", 81));
    currencyDataList.push_back(make_tuple("INR", "JPY", 50));

    CurrencyConversionCalculator currencyConversionCalculator = *new CurrencyConversionCalculator();

    for(converterTuple val: currencyDataList){
        string fromCountry = get<0>(val);
        string toCountry = get<1>(val);
        float value = get<2>(val);

        currencyConversionCalculator.addCurrencyConversion(fromCountry, toCountry, value);
    }

    cout<<currencyConversionCalculator.findCurrencyConversion("INR", "JPY", 4)<<endl;
    cout<<currencyConversionCalculator.findCurrencyConversion("USD", "JPY", 4)<<endl;
    cout<<currencyConversionCalculator.findCurrencyConversion("IND", "JPY", 4)<<endl;
    cout<<currencyConversionCalculator.findCurrencyConversion("IND", "USD", 4)<<endl;
    cout<<currencyConversionCalculator.findCurrencyConversion("INR", "USD", 1)<<endl;

}