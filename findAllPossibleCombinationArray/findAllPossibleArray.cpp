//
// Created by Atul Mishra on 22/07/23.
//

/*
Given S = {2,1};
find all possible array A that can be formed such that :
 - A[i] < A[i+1]
 - sum of all digits at A[i] = S[i]
 - 1 <= A[i] <= 1000
 - 1 <= S[i] <= 30
 - 1 <= len(S) <= 10

Return all such combination as ans = ans % 10^9
*/

#include "iostream"
#include "map"
using namespace std;

unordered_map<int, vector<int>> preCalculateAllPossibleSum(){
    unordered_map<int, vector<int>> digitSumMap;
    for(int i = 1; i<= 1000; i++){
        int temp = i;
        int sum = 0;
        while(temp){
            sum += (temp % 10);
            temp = temp/10;
        }
        digitSumMap[sum].push_back(i);
    }

    return digitSumMap;
}

int findAllPossibleCombinations(int index, int val, unordered_map<int,
        vector<int>>& digitSumMap, vector<int>& S,
        vector<vector<int>>& dp){
    if(index == S.size()){
        return  1;
    }

    if(dp[index][val] != -1){
        return dp[index][val];
    }

    int currentS = S[index];
    vector<int> digitSumVector = digitSumMap[currentS];
    int lb = lower_bound(digitSumVector.begin(), digitSumVector.end(), val) - digitSumVector.begin();
    int ans = 0;
    for(int i = lb; i<digitSumVector.size(); i++){
        ans = (ans + findAllPossibleCombinations(index+1, digitSumVector[i], digitSumMap, S, dp));
    }
    return dp[index][val] = ans;
}

int main(){
    vector<int> S = {2,1,5};
    unordered_map<int, vector<int>> digitSumMap = preCalculateAllPossibleSum();

    vector<vector<int>> dp(101, vector<int>(1001, -1));
    cout<<findAllPossibleCombinations(0, 0 , digitSumMap, S, dp);

}