//
// Created by Atul Mishra on 22/07/23.
//

#include "iostream"
#include "vector"
using namespace std;

void findAllSubsequences(vector<int> input, vector<vector<int>>& allSubsequences, vector<int> currentSequence, int index){
    if(index == input.size()){
        if(!currentSequence.empty()){
            return allSubsequences.push_back(currentSequence);
        }
    } else {
        currentSequence.push_back(input[index]);
        findAllSubsequences(input, allSubsequences, currentSequence, index+1);
        currentSequence.pop_back();
        findAllSubsequences(input, allSubsequences, currentSequence, index+1);
    }
}

int main(){
    vector<int> input = {1,2,3,4};
    vector<vector<int>> allSubsequnces;
    findAllSubsequences(input, allSubsequnces, {}, 0);
    for(auto sequence: allSubsequnces){
        for(int i: sequence){
            cout<<i<<" ";
        }
        cout<<endl;
    }
}