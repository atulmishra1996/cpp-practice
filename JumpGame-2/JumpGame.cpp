//
// Created by Atul Mishra on 22/07/23.
//

/*
 *  https://leetcode.com/problems/jump-game-ii/description/
 *
 *
*/


#include "iostream"
#include "vector"
#include "regex"
using namespace std;


//int getMinimumJumpRequired(vector<int>& nums, int index, vector<int>& dp){
//
//}

bool isBlocked(const std::string& ip, const std::vector<std::string>& blockedIPs) {
    for (const auto& blockedIP : blockedIPs) {
        // Convert blocked IP to a regex pattern
        std::string regexPattern = blockedIP;
        // Replace "*" with a regex wildcard ".+"
        size_t pos = regexPattern.find("*");
        while (pos != std::string::npos) {
            regexPattern.replace(pos, 1, ".+");
            pos = regexPattern.find("*", pos + 2);
        }

        // Check if the current blocked IP matches the input IP
        std::regex pattern(regexPattern);
        if (std::regex_match(ip, pattern)) {
            return true;
        }
    }

    return false;
}

int main(){
    std::vector<std::string> blacklist = {
            "111.*.255", // Matches any IP address starting with "111." and ending with ".255"
            "12.*",        // Matches any IP address starting with "12."
            "123.*",
            "*111.*",
            "34.*"
    };

    std::vector<std::string> IPs = {
            "121.3.5.255",
            "12.13.5.255",
            "111.3.5.255",
            "121.3.5.255",
            "123.1.23.34",
            "121.1.23.34",
            "121.1.23.34",
            "34.1.23.34",
            "121.1.23.34",
            "12.1.23.34",
            "121.1.23.34"
    };

    for (const auto& ip : IPs) {
        if (isBlocked(ip, blacklist)) {
            std::cout << "IP address " << ip << " is blocked." << std::endl;
        } else {
            std::cout << "IP address " << ip << " is not blocked." << std::endl;
        }
    }
}