//
// Created by Atul Mishra on 19/07/23.
//
#include "iostream"
#include "map"
using namespace std;

class MyClass {
public:
    int priority;
    int nextPriority;
    std::string name;

    // Constructor
    MyClass(int p, int np,const std::string& n) : priority(p), nextPriority(np), name(n) {}

    // Comparison operator for std::map (custom comparison)
    bool operator<(const MyClass& other) const {
        if(priority == other.priority){
            return nextPriority > other.nextPriority;
        }
        // We define the comparison based on the priority value
        return priority < other.priority;
    }
};

int main() {
    std::map<MyClass, int> myMap;

    // Creating objects of MyClass with different priorities
    MyClass obj1(3, 4, "Object 1");
    MyClass obj2(1, 5, "Object 2");
    MyClass obj3(1, 6,"Object 3");

    // Inserting the objects into the map with priority as the key
    myMap[obj1] = 1;
    myMap[obj2] = 1;
    myMap[obj3] = 1;

    // Accessing elements in the map (they will be sorted by priority)
    for (const auto& pair : myMap) {
        const MyClass& keyObj = pair.first;
        int value = pair.second;
        std::cout << "Name: " << keyObj.name << ", Priority: " << keyObj.priority <<", Next Priority: " << keyObj.nextPriority<< ", Value: " << value << std::endl;
    }

    return 0;
}