//
// Created by Atul Mishra on 21/07/23.
//

#include "iostream"
#include "string"

using namespace std;

int main(){
    string test = "1100";
    int k = 5;
    int testLen = test.length();
    vector<int> countOfZero(testLen, 0);
    vector<int> countOfOne(testLen, 0);
    vector<int> diff(testLen, 0);

    if(test[0] == '1')
    {
        countOfOne[0] = 1;
    } else {
        countOfZero[0] = 1;
    }

    for(int i=1; i<testLen;i++){
        if(test[i] == '0'){
            countOfZero[i] = countOfZero[i-1] + 1;
            countOfOne[i] = countOfOne[i-1];
        } else {
            countOfZero[i] = countOfZero[i - 1];
            countOfOne[i] = countOfOne[i - 1] + 1;
        }
    }


    for(int i=0;i<testLen;i++){
        diff[i] = countOfZero[i] - countOfOne[i];
    }

    bool doesKExist = false;
    for(int i: diff){
        if(i == k)doesKExist = true;
    }

    if(diff[testLen - 1] == 0 && !doesKExist) {
        cout<<-1<<endl;
    } else {

        int count = 0;
        int incrementingValue = diff[testLen - 1];
        for (int i: diff) {
            int x = k - (i);
            if (x % incrementingValue == 0)count++;
        }

        cout << count;
    }
}
