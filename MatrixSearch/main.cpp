#include "iostream"
#include "vector"

using namespace std;

class Solution {
public:

    int findExpectedRow(vector<vector<int>>& matrix, int target){
        int n = matrix.size();
        int m = matrix[0].size();

//        cout<<n<<" :: "<<m<<endl;
        int l = 0;
        int r = n-1;
        int mid = (l+r)/2;

        while(l <= r) {
            mid = (l+r)/2;
            if(matrix[mid][m-1] == target){
                return mid;
            } else if (matrix[mid][m-1] > target){
                r = mid -1;
            } else {
                l = mid + 1;
            }
        }

        return mid;
    }


    bool isTargetExist(vector<int> expectedRow, int target){
        int n = expectedRow.size();
        int l = 0;
        int r = n-1;

        int mid = (l+r)/2;

        while(l <= r){
            mid = (l+r)/2;

            if(expectedRow[mid] == target) {
                return true;
            } else if(expectedRow[mid] > target){
                r = mid - 1;
            } else {
                l = mid + 1;
            }
        }
        if(expectedRow[l] == target || expectedRow[r] == target){
            return true;
        }
        return false;
    }

    bool searchMatrix(vector<vector<int>>& matrix, int target) {
        int expectedRow = findExpectedRow(matrix, target);
//        cout<<expectedRow<<endl;
        return isTargetExist(matrix[expectedRow], target);
    }
};

int main(){
    Solution solution = *new Solution();
    vector<vector<int>> matrix = {
            {1,3,5,7},
            {10,11,16,20},
            {23,30,34,60}
    };

    cout<<boolalpha<< (solution.searchMatrix(matrix, 2));
 }
