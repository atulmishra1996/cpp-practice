//
// Created by Atul Mishra on 19/07/23.
//
/*
Create a cache that works on LFU and priority assigned to an account ID.
cache limit 5
User Inputs  - Item id/name & Account Name
        Example of representation:

Items | ItemName | Account | fq
        I1 | ItemA | SST | 5
I2 | ItemB | REL | 3
I3 | ItemC | MOR | 6
I4 | ItemD | DAM  | 2

Account priority
SST p0
REL p1
MOR p2
DAM p3

Functions to be implemented
Get
        Add
PrintAll

        Take inputs from the CLI and call the functions

struct ItemDetails {
    string itemID;
    string itemName;
    string account;
}

get(itemId) -> ItemName ans increment frequency, Notfound
Add(ItemDetails itemDetails)

printAll() -> prints All Data
*/

#include "iostream"
#include "queue"
#include "map"
#include "string"

using namespace std;

class ItemDetails {
public:
    string itemId;
    string itemName;
    int accountPriority;
    int frequency;

    ItemDetails(){}

    ItemDetails(string itemI, string itemNam, string accountId, int frequenc){
        this->itemId = itemI;
        this->itemName = itemNam;
        this->frequency = frequenc;
        setAccountPriority(accountId);
    }

    void setAccountPriority(string &accountId){
        if(accountId == "s1"){
            this->accountPriority = 0;
        } else if(accountId == "s2"){
            this->accountPriority = 1;
        } else if(accountId == "s3"){
            this->accountPriority = 2;
        } else if(accountId == "s4"){
            this->accountPriority = 3;
        } else {
            cout<<"Wrong priority given :: auto assigning priority to 4"<<endl;
            this->accountPriority = 4;
        }
    }

    string getAccountId(){
        if(frequency == 0){
            return "s1";
        } else if(frequency == 1){
            return "s2";
        } else if(frequency == 2){
            return "s3";
        } else if(frequency == 3){
            return "s4";
        } else {
            return "Unknown";
        }
    }

    bool operator<(const ItemDetails &otherItem) const {
        if(frequency == otherItem.frequency){
            return accountPriority > otherItem.accountPriority;
        }
        return frequency < otherItem.frequency;
    }
};

class LFUCache {
public:
    map<string, ItemDetails> itemDetailsMap;
    int cacheSize;

    LFUCache(int size){
        this->cacheSize = size;
        this->itemDetailsMap.clear();
    }

    void printAll(){
        cout<<"Item ID  || Item Name || Account Id || Frequency"<<endl;

        for(auto &pair: itemDetailsMap){
            ItemDetails itemDetails = pair.second;
            cout<<itemDetails.itemId<<" | "<<itemDetails.itemName<<" | "<<itemDetails.getAccountId()<<" | "<<itemDetails.frequency<<endl;
        }
    }

    void getItem(string itemId){
        if(itemDetailsMap.find(itemId) == itemDetailsMap.end()){
            cout<<"Item id not found"<<endl;
        } else {
            map<string, ItemDetails>::iterator it;
            it = itemDetailsMap.find(itemId);
            ItemDetails itemDetails = it->second;
            cout<<itemDetails.itemId<<" | "<<itemDetails.itemName<<" | "<<itemDetails.getAccountId()<<" | "<<itemDetails.frequency + 1<<endl;
            itemDetailsMap.erase(itemId);

            ItemDetails itemDetails1 = *new ItemDetails(itemDetails.itemId, itemDetails.itemName, itemDetails.getAccountId(), itemDetails.frequency + 1);
            itemDetailsMap.emplace(itemId, itemDetails1);
//            itemDetailsMap[itemId] = itemDetails;

        }
    }

    void removeLeastFrequentlyUsedItem(){
        map<string, ItemDetails>::iterator it;
        it = itemDetailsMap.begin();
        string itemId = it->first;
        itemDetailsMap.erase(itemId);
    }

    void addNewItem(ItemDetails itemDetails){
        itemDetailsMap[itemDetails.itemId] = itemDetails;
        itemDetailsMap.emplace(itemDetails.itemId, itemDetails);
//        itemDetailsMap[itemDetails.itemId] = itemDetails;
    }

    void addItem(ItemDetails itemDetails){
        if(itemDetailsMap.size() == this->cacheSize){
            removeLeastFrequentlyUsedItem();
        }
        addNewItem(itemDetails);
    }
};

void addItem(LFUCache &lfuCache){
    string itemId, itemName, accountId;
    cout<<"Enter Item ID"<<endl;
    cin>>itemId;
    cout<<"Enter Item Name"<<endl;
    cin>>itemName;
    cout<<"Enter Item Account ID :: s1 | s2 | s3 | s4 "<<endl;
    cin>>accountId;
    ItemDetails itemDetails = *new ItemDetails(itemId, itemName, accountId, 0);
    lfuCache.addItem(itemDetails);
}

void getItem(LFUCache &lfuCache){
    string itemId;
    cout<<"Enter Item ID"<<endl;
    cin>>itemId;
    lfuCache.getItem(itemId);
}

int main(){
    int cacheSize;
    cout<<"Enter Cache Size"<<endl;
    cin>>cacheSize;
    LFUCache lfuCache = *new LFUCache(cacheSize);
    while(true){
        int cmd;
        cout<<"Enter CMD  | 0 : printAll | 1 : addItem | 2: getItem "<<endl;
        cin>>cmd;
        switch (cmd) {
            case 0:
                lfuCache.printAll();
                break;
            case 1:
                addItem(lfuCache);
                break;
            case 2:
                getItem(lfuCache);
                break;
            default :
                break;
        }
    }
}